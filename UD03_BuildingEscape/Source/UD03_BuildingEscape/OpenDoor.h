// (C) steamlead

#pragma once

#include "Components/ActorComponent.h"
#include "OpenDoor.generated.h"
#define OUT //Does nothing, just used to mark output parameters

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FDoorEvent);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )

class UD03_BUILDINGESCAPE_API UOpenDoor : public UActorComponent
{
	GENERATED_BODY()
public:
	// Sets default values for this component's properties
	UOpenDoor();

	//Return total mass in kg
	float GetTotalOfActorsOnPlate();

	// Called when the game starts
	virtual void BeginPlay() override;

	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UPROPERTY(BlueprintAssignable)
	FDoorEvent OnOpen;

	UPROPERTY(BlueprintAssignable)
	FDoorEvent OnClose;

private:
	UPROPERTY(EditAnywhere)
	ATriggerVolume* PressurePlate=nullptr;

	UPROPERTY(EditAnywhere)
	float DoorClosedelay = 1.0f;

	UPROPERTY(EditAnywhere)
	float TriggerMass = 50.0f;

	//UPROPERTY(EditAnywhere)
	AActor* Owner = nullptr;;
};
