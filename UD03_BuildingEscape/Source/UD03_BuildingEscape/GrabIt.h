// (C) steamlead

#pragma once

#include "Components/ActorComponent.h"
#include "GrabIt.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class UD03_BUILDINGESCAPE_API UGrabIt : public UActorComponent
{
	GENERATED_BODY()

public:
	// Sets default values for this component's properties
	UGrabIt();
	void Grab();
	void Release();
	// Called when the game starts
	virtual void BeginPlay() override;

	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UPROPERTY(VisibleAnywhere)
	float ZModifier = 20.0f;

	UPROPERTY(EditAnywhere)
	ATriggerVolume* PressurePlate;

	APawn* Player;
	AActor* Owner;
	bool IsGrabbed;
	FVector ActorLocation;
	FVector PlayerLocation;
	FVector DiffLocation;
};
